import { NavBar } from '@/components';
import type { NextPage } from 'next'
import Head from 'next/head'

const Home: NextPage = () => {
  return (
    <div className=''>
      <Head>
        <title>Duy Nguyen's Portfolio Project</title>
        <meta name="description" content="Duy Nguyen's Portfolio Project" />
        <link rel="icon" href="/moon.ico" />
      </Head>
      <NavBar/>
    </div>
  );
}

export default Home

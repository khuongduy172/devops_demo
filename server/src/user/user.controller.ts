import { Controller, Get, Req, UseGuards } from "@nestjs/common";
import { JwtGuard } from "../auth/guard";
import { GetUser } from "../auth/decorator";
import { UserService } from "./user.service";

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}
  
  @UseGuards(JwtGuard)
  @Get('me')
  getMe (@GetUser('userId') userId) { 
    return this.userService.getMe(userId);
  }

  @Get()
  get () { 
    return this.userService.getAll();
  }
}
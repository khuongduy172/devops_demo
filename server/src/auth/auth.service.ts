import { PrismaService } from './../prisma/prisma.service';
import { ForbiddenException, Injectable } from "@nestjs/common";
import { AuthDto } from '../dto';
import * as argon from 'argon2';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable({})
export class AuthService {
  constructor(
    private config: ConfigService,
    private prisma: PrismaService, 
    private jwt: JwtService,
  ) {}

  async signup(dto: AuthDto) {
    try {
      // generate hash for password
      const hash = await argon.hash(dto.password);

      // save user to database
      const user = await this.prisma.user.create({
        data: { email: dto.email, hash },
      });

      // return token
      return await this.signToken(user.id, user.email);
    } catch (error) {
      if (error.code === 'P2002') {
        throw new ForbiddenException('User already exists');
      }
    }
  }

  async signin(dto: AuthDto) {
    try {
      // get user from database
      const user = await this.prisma.user.findUnique({
        where: { email: dto.email },
      });

      // check if user exists
      if (!user) {
        throw new ForbiddenException('User does not exist');
      }

      // check if password is correct
      const valid = await argon.verify(user.hash, dto.password);
      if (!valid) {
        throw new ForbiddenException('Invalid password');
      }

      // return token
      return await this.signToken(user.id, user.email);
    } catch (error) {
      if (error.code === 'P2002') {
        throw new ForbiddenException('User does not exist');
      }
    }
  }

  async signToken(userId: number, email: string) {
    const payload = {
      userId: userId,
      email: email,
    };

    const secret = this.config.get('JWT_SECRET');
    const token = await this.jwt.signAsync(payload, { expiresIn: '1h', secret: secret });
    return { accessToken: token };
  }
}